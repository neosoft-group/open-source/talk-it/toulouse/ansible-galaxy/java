.SILENT:
.ONESHELL:
SHELL:=/bin/bash

.PHONY: bootstrap
bootstrap:
	python3 -m venv virtualenv
	source virtualenv/bin/activate
	pip install --upgrade pip setuptools wheel
	pip install --requirement requirements.txt

	ansible --version

.PHONY: reinitialization
reinitialization:
	rm -rf virtualenv
	$(MAKE) bootstrap

.PHONY: molecule-test
molecule-test:
	source virtualenv/bin/activate
	molecule test

.PHONY: molecule-converge
molecule-converge:
	source virtualenv/bin/activate
	molecule converge

.PHONY: molecule-destroy
molecule-destroy:
	source virtualenv/bin/activate
	molecule destroy

.PHONY: molecule-verify
molecule-verify:
	source virtualenv/bin/activate
	molecule verify
